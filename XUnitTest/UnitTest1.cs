using RotateArray;
using System;
using Xunit;

namespace XUnitTest
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            int K = 3;
            int[] input =  { 3, 8, 9, 7, 6 };
            int[] output = { 9, 7, 6, 3, 8 };
            Assert.Equal(output, Program.Solution(input, K));
        }

        [Fact]
        public void Test2()
        {
            int K = 1;
            int[] input =  { 0, 0, 0 };
            int[] output = { 0, 0, 0 };
            Assert.Equal(output, Program.Solution(input, K));
        }

        [Fact]
        public void Test3()
        {
            int K = 4;
            int[] input =  { 1, 2, 3, 4 };
            int[] output = { 1, 2, 3, 4 };
            Assert.Equal(output, Program.Solution(input, K));
        }
    }
}
