﻿using System;

namespace RotateArray
{
    public class Program
    {
        public static void Main()
        {
            Console.WriteLine("Enter array index :");
            int n = Convert.ToInt32(Console.ReadLine());
            int[] A = new int[n];
            Console.WriteLine("Please Enter ",+n," Floating numbers");
            for (int i = 0; i < n; i++)
            {
                A[i] = Convert.ToInt32(Console.ReadLine());
            }
            Console.WriteLine("Enter a Number :");
            int K = Convert.ToInt32(Console.ReadLine());
            int[] res = Solution(A, K);

            for (int i = 0; i < res.Length; i++)
            {
                if (i != res.Length-1)
                {
                    Console.Write(res[i] + ",");
                }
                else
                    Console.Write(res[i]);
            }
            Console.ReadLine();
        }

        public static int[] Solution(int[] A, int K)
        {
            int[] B = new int[A.Length];
            for (int l = 0; (K > l); K--)
            {
                int j = 0;
                for (int i = 0; (i < A.Length); i++)
                {
                    if ((i == 0))
                    {
                        B[j] = A[(A.Length - 1)];
                        j++;
                    }
                    else
                    {
                        B[j] = A[(i - 1)];
                        j++;
                    }

                }

                for (int i = 0; (i < A.Length); i++)
                {
                    A[i] = B[i];
                }

            }

            return B;

            //for (int i = 0; i < K; i++)
            //{
            //    K = K % A.Length;
            //    int[] array = new int[K];
            //    Array.Copy(A, A.Length - K, array, 0, K);
            //    Array.Copy(A, 0, A, K, A.Length - K);
            //    Array.Copy(array, A, K);
            //}
            //return A;
        }
    }
}
